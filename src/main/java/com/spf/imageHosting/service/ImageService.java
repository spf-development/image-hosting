package com.spf.imageHosting.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

@Service
public class ImageService {

    @Value("${image.path}")
    String imagePath;

    @EventListener(ApplicationReadyEvent.class)
    public void chekAfterStart() {
        File uploadDir = new File(imagePath);
        if (!uploadDir.exists()) {
            uploadDir.mkdir();
        }

    }

    public String add(MultipartFile file) throws IOException {
        if (file != null) {
            String uuid = UUID.randomUUID().toString();
            String insPath = imagePath + "/" + uuid;

            Files.write(Paths.get(insPath), file.getBytes());
            return uuid;
        }
        return "File is absent";
    }

    public ResponseEntity<byte[]> getImage(String uuid) {
        String path = imagePath + "/" + uuid;
        final File initialFile = new File(path);

        try (ByteArrayOutputStream bao = new ByteArrayOutputStream();
             InputStream is = new DataInputStream(new FileInputStream(initialFile))) {

            BufferedImage img = ImageIO.read(is);
            ImageIO.write(img, "jpg", bao);
            byte[] media = bao.toByteArray();

            ResponseEntity<byte[]> responseEntity = new ResponseEntity<>(media, HttpStatus.OK);
            return responseEntity;
        } catch (FileNotFoundException e) {
            ResponseEntity<byte[]> responseEntity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
            return responseEntity;
        } catch (IOException e) {
            ResponseEntity<byte[]> responseEntity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            return responseEntity;
        }
    }

    public String delete(String uuid) {
        String path = imagePath + "/" + uuid;
        final File initialFile = new File(path);

        if(initialFile.delete())
            return "Ok";
        else
            return "File is not deleted";
    }
}
