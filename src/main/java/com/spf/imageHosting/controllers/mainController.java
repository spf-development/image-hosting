package com.spf.imageHosting.controllers;

import com.spf.imageHosting.service.ImageService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@AllArgsConstructor
public class mainController {

    ImageService imageService;

    @GetMapping("/image")
    public ResponseEntity<byte[]> getImage(@RequestParam("uuid") String uuid) {
        return imageService.getImage(uuid);
    }

    @PostMapping("/add")
    public String add(@RequestParam("file") MultipartFile file) throws IOException {
        return imageService.add(file);
    }

    @PostMapping("/delete")
    public String delete(@RequestParam("uuid") String uuid) {
        return imageService.delete(uuid);
    }
}
